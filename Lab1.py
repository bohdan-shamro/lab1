import matplotlib.pyplot as plt
import random


def MX(arr):
    MX = 0
    for i in arr:
        MX += i
    return MX / len(arr)


def MX2(arr):
    MX2 = 0
    for i in arr:
        MX2 += i ** 2
    return MX2 / len(arr)


def DX(one, two):
    DX = 0
    return one - two ** 2


def printing(arr, a, b, c):
    q = [random.randint(1, 10) for _ in range(0, len(arr))]
    plt.title("Scatter")
    plt.xlabel("Our points")
    plt.ylabel("random numbers")
    plt.scatter(arr, q, color="blue",label=' our point')
    plt.scatter(a, random.randint(0, 10), color='yellow',label= "MX")
    plt.scatter(b, random.randint(0, 10), color='black', label="MX2")
    plt.scatter(c, random.randint(0, 10), color='red',label="DX")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    results = [-14.82381293, -0.29423447, -13.56067979, -1.6288903, -0.31632439,
               0.53459687, -1.34069996, -1.61042692, -4.03220519, -0.24332097]
    a = MX(results)
    b = MX2(results)
    c = DX(b, a)
    print("MX = ", a)
    print("MX2 = ", b)
    print("Dispersion", c)
    print(printing(results, a, b, c))
